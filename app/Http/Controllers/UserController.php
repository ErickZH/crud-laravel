<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Exception;

class UserController extends Controller
{

    private $path = 'user';

    public function index()
    {
        // Traemos todos los registros de los usuarios
        $data = User::all();

        // Enviamos registros a la vista
        return view($this->path.'.index', compact('data'));
    }

    public function create()
    {
        return view($this->path.'.create');
    }

    public function store(Request $request)
    {
        // Registrar usuario
        try
        {
            $user = new User();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            return redirect()->route('users.index');
        }
        catch (Exception $e)
        {
            return "Fatal error - ".$e->getMessage();
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //Editar usuario
        $user = User::findOrFail($id);

        return view($this->path.'.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        // Actualizamos info del usuario
        $user = User::findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        //Eliminar usuario
        try
        {
            $user = User::findOrFail($id);

            $user->delete();

            return redirect()->route('users.index');
        }
        catch (Exception $e)
        {
            return "Fatal error - ".$e->getMessage();
        }

    }
}
